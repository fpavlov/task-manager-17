package ru.t1.fpavlov.tm.command.entity.project;

import ru.t1.fpavlov.tm.enumerated.Status;
import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectCompleteByIndexCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Complete project by index";

    public static final String NAME = "project-complete-by-index";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Project entity = this.findByIndex();
        this.getProjectService().changeStatus(entity, Status.COMPLETED);
    }

}
