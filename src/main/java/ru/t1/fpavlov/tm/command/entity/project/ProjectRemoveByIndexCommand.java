package ru.t1.fpavlov.tm.command.entity.project;

import ru.t1.fpavlov.tm.model.Project;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    public static final String DESCRIPTION = "Remove project by index";

    public static final String NAME = "project-remove-by-index";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        final Project entity = this.findByIndex();
        this.getProjectTaskService().removeProject(entity);
    }

}
