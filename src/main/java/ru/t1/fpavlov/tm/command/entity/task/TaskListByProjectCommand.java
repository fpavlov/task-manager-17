package ru.t1.fpavlov.tm.command.entity.task;

import ru.t1.fpavlov.tm.enumerated.Sort;
import ru.t1.fpavlov.tm.model.Task;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class TaskListByProjectCommand extends AbstractTaskCommand {

    public static final String DESCRIPTION = "List task by project id";

    public static final String NAME = "task-list-by-project-id";

    @Override
    public String getArgument() {
        return this.ARGUMENT;
    }

    @Override
    public String getDescription() {
        return this.DESCRIPTION;
    }

    @Override
    public String getName() {
        return this.NAME;
    }

    @Override
    public void execute() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        final Sort sort = this.askEntitySort();
        final List<Task> tasks = this.getTaskService().findAllByProjectId(projectId, sort);
        this.renderEntities(tasks, "Tasks:");
    }

}
