package ru.t1.fpavlov.tm.comparator;

import ru.t1.fpavlov.tm.api.model.IHasStatus;

import java.util.Comparator;

/**
 * Created by fpavlov on 26.11.2021.
 */
public enum StatusComparator implements Comparator<IHasStatus> {

    INSTANCE;

    @Override
    public int compare(final IHasStatus item1, final IHasStatus item2) {
        if (item1 == null || item2 == null) return 0;
        if (item1.getStatus() == null || item2.getStatus() == null) return 0;
        return item1.getStatus().compareTo(item2.getStatus());
    }

}
