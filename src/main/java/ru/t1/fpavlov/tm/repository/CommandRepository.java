package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.repository.ICommandRepository;
import ru.t1.fpavlov.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;


/*
 * Created by fpavlov on 04.10.2021.
 */
public final class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> mapByArgument = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> mapByName = new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        final String name = command.getName();
        if (name != null && !name.isEmpty()) this.mapByName.put(name, command);
        final String argument = command.getArgument();
        if (argument != null && !argument.isEmpty()) this.mapByArgument.put(argument, command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String commandArgument) {
        if (commandArgument == null || commandArgument.isEmpty()) return null;
        return this.mapByArgument.get(commandArgument);
    }

    @Override
    public AbstractCommand getCommandByName(final String commandName) {
        if (commandName == null || commandName.isEmpty()) return null;
        return this.mapByName.get(commandName);
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return this.mapByName.values();
    }

}
