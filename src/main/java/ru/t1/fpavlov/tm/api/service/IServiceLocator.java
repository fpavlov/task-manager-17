package ru.t1.fpavlov.tm.api.service;

/**
 * Created by fpavlov on 07.12.2021.
 */
public interface IServiceLocator {

    ICommandService getCommandService();

    ILoggerService getLoggerService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

}
